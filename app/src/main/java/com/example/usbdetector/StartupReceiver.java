package com.example.usbdetector;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.PersistableBundle;
import android.provider.Settings;
import android.util.Log;

import java.util.concurrent.TimeUnit;

public class StartupReceiver extends BroadcastReceiver {

    private static final String TAG = "StartupReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive:");

        Intent intentActivity = new Intent(context, MainActivity.class);
        //intentActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intentActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intentActivity);
        Log.d(TAG, "onReceive end:");
    }

}
