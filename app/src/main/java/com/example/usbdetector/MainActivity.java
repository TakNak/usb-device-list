package com.example.usbdetector;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.RestrictionsManager;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Iterator;

import static java.lang.Integer.toHexString;

public class MainActivity extends AppCompatActivity {

    private static String TAG = "UsbDetector";
    private USBReceiver mReceiver;
    private static UsbManager manager;
    private static TextView testView;
    private static TextView textView2;
    PowerManager.WakeLock wakeLock;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mReceiver = new USBReceiver();

        IntentFilter filter = new IntentFilter();

        {
            filter.addAction(UsbManager.ACTION_USB_ACCESSORY_ATTACHED);
            filter.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);
            filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
            filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
            filter.addAction("android.hardware.usb.action.USB_STATE");
            filter.addAction("android.hardware.usb.action.USB_ACCESSORY_ATTACHED");
            registerReceiver(mReceiver, filter);
        }
        manager = (UsbManager) getSystemService(Context.USB_SERVICE);
        testView = findViewById(R.id.textView);
        textView2 = findViewById(R.id.textView2);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "MyApp::MyWakelockTag");
        wakeLock.acquire();
    }

    @Override
    protected void onResume() {
        super.onResume();

        showUSBList(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unregisterReceiver(mReceiver);

        wakeLock.release();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }


    public static void showUSBList(Intent intent) {
        HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
        UsbAccessory[] accessory = manager.getAccessoryList();

        String str = "";
        if(accessory != null) {
            String manufacture = accessory[0].getManufacturer();
            str = "accessory:" + accessory + "manufacture:" + manufacture +"\n";
            Log.d(TAG, str);
            textView2.setText(str);
        }

        if(true){
            Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();
            if (intent != null) {
                Log.d(TAG, "showUSBList intetn");
                str = "";
                str += (String) textView2.getText();
                str += intent.getAction().toString();

                if (deviceIterator.hasNext()) {
                    UsbDevice device = deviceIterator.next();
                    str += ",vid:0x" + toHexString(device.getVendorId()) + ",pid:0x" + toHexString(device.getProductId())+ "\n";
                }
                str += "\n";
                textView2.setText(str);
            }
        }

        Log.d("USBList", "start");

        Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();
        str = "";
        int endpoint_total_in = 0;
        int endpoint_total_out = 0;
        while(deviceIterator.hasNext()){
            UsbDevice device = deviceIterator.next();
            //Log.d("USBList",device.toString());
            Log.d("UsbList", device.getDeviceName());
            Log.d("VendorId", "id:0x" +toHexString(device.getVendorId()));
            Log.d("ProductId", "id:0x" +toHexString(device.getProductId()));
            str += "deviceName:" + device.getDeviceName();
            str += "VendorId:0x" + toHexString(device.getVendorId());
            str += "ProductId:0x" + toHexString(device.getProductId()) + "\n";
            for(int i= 0; i < device.getConfigurationCount(); i++ ) {
                //str += "Configuration:" + device.getConfiguration(i).toString() + "\n";
            }
            for(int i= 0; i < device.getInterfaceCount(); i++) {
                UsbInterface _interface = device.getInterface(i);

                //str += "interface:" + _interface.toString() + "\n";

                for(int j=0; j < _interface.getEndpointCount(); j++) {
                    UsbEndpoint _endpoint = _interface.getEndpoint(j);

                    HashMap<Integer, String> _dirString = new HashMap<>();
                    _dirString.put(128, "IN");
                    _dirString.put(0, "OUT");

                    if(_endpoint.getDirection() == 128) {
                        endpoint_total_in += 1;
                    }
                    else {
                        endpoint_total_out += 1;
                    }

                    //str += "direction:" + _dirString.get(_endpoint.getDirection()) + ", number:" + _endpoint.getEndpointNumber() + "\n";
                }
            }
        }

        str += "endpoint total IN:" + endpoint_total_in + "\n";
        str += "endpoint total OUT:" + endpoint_total_out + "\n";

        testView.setText(str);

        Log.d("USBList", "finish");
    }

}
